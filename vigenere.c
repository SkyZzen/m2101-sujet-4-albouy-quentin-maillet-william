	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <math.h>
	#include <wchar.h>
	#include <locale.h>
	#include <wctype.h>
	#include<time.h>
	#include "projet.h"


void vigenere(wchar_t * test ){
	FILE* fichier = NULL;
	fichier = fopen("Resultat.txt", "a");
	wchar_t reponse[10];
	wchar_t crypt[2001]; 			  
	int nombre[26] = {0};
	float ind=0;
	int s;
	wchar_t clef[10];
	wchar_t contenu[200];
	int i = 0;
	wchar_t * mode;
	for (i = 0 ; test[i] != '\0' ; i++)
    {
      contenu[i] = towlower(contenu[i]);
    }
	printf("Tapez (c) pour Crypter et (d) pour decrypter: \n");
	printf("---------------------------------------------\n");
	   
	lire(reponse,10);
	
	if (wcspbrk (reponse,L"cC")){
		s = 1;
		mode = L"crypter";
	} else if(wcspbrk (reponse,L"dD")) {
		s = -1;
		mode = L"decrypter";
	} else {
		printf("Mauvais choix, Aurevoir !\n");
		exit(-1);
	}
	
	 
	printf("Veuilliez entrez la clef \n");
	lire(clef,10);
	if (fichier != NULL)
	{
    	fprintf(fichier,"Le texte va etre %ls .\n La clef utilisée est %ls .\n",mode,clef);
    	fclose(fichier);
	}   
	cryp_decryp(test, clef, crypt, s);

    printf("Le resultat du texte correspondant a cette clef est:\n");
	printf("------------------------------------------------------\n");
	printf("%ls\n", crypt);
	if (fichier != NULL)
	{
    	fprintf(fichier,"Le Resultat est %ls\n",crypt);
    	fclose(fichier);
	}   

}