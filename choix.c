	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <math.h>
	#include <wchar.h>
	#include <locale.h>
	#include <wctype.h>
	#include<time.h>
	#include "projet.h"
	
void choix (wchar_t * test){
	FILE* fichier = NULL;
	fichier = fopen("Resultat.txt", "a");
	char * rep1;
	wchar_t rep[10];
	printf ( "Voulez-vous Crypter/Decrypter avec Vigenère ou César ? \n" );
	printf ( "(C) pour César et (V) pour Vigenère\n" );
	lire(rep,10);
	if (wcspbrk (rep,L"cC")){
		if (fichier != NULL)
	{
    	fprintf(fichier,"Le mode de Cryptage/Decryptage est César\n");
    	fclose(fichier);
	}
	cesar(test);
	
		
	} else if(wcspbrk (rep,L"vV"))  {
		if (fichier != NULL)
	{
    	fprintf(fichier,"Le mode de Cryptage/Decryptage est Vigenère\n");
    	fclose(fichier);
	}
		vigenere(test);
	}
    else {printf("Mauvais choix, Aurevoir !\n");
			exit(-1);
		}
}
