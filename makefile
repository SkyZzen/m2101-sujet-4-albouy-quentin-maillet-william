all: main

main: main.c choix.o desaccentuer.o vigenere.o cesar.o cryptercesar.o decryptercesar.o cryp_decryp.o lire.o
	gcc main.c choix.o desaccentuer.o vigenere.o cesar.o cryptercesar.o decryptercesar.o cryp_decryp.o lire.o -o main
	
choix.o: choix.c
	gcc -c choix.c
	
desaccentuer.o: desaccentuer.c
	gcc -c desaccentuer.c
	
vigenere.o: vigenere.c
	gcc -c vigenere.c
	
cesar.o: cesar.c
	gcc -c cesar.c

cryptercesar.o: cryptercesar.c
	gcc -c cryptercesar.c
	
decryptercesar.o: decryptercesar.c
	gcc -c decryptercesar.c
	
cryp_decryp.o: cryp_decryp.c
	gcc -c cryp_decryp.c

lire.o: lire.c
	gcc -c lire.c

clean: 
	rm main
	rm *.o