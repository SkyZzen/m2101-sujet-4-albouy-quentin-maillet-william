 
Projet 4: Chiffrage de messages


Question 1:

Algorithme “César”:
	L’algorithme César est un algorithme de chiffrement qui consiste à remplacer chaque lettre du message original par une autre à distance fixe dans l’ordre de l’alphabet. Cependant, son nombre de clés très limitées (25) fait de lui un algorithme pouvant être cassé très facilement, même par un humain.
	Exemple: Avec un décalage de 3, HELLO WORLD s’écrit KHOOR ZRUOG. 

Algorithme “Vigenère”:
	L’algorithme Vigenère est un algorithme de chiffrement qui utilise une clé. Cette clé est répétée jusqu’à faire la taille du message afin de remplacer chaque lettre du message par une autre décalée de la place dans l’alphabet des lettres de la clé. Cet algorithme étant plus poussé, il est plus difficile à casser, de plus, plus la clé est longue et plus le message est protégé.
	Exemple: Avec MDP comme clé de chiffrement, HELLO WORLD s’écrit THAXR LAUAP.