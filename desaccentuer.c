	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <math.h>
	#include <wchar.h>
	#include <locale.h>
	#include <wctype.h>
	#include<time.h>
	#include "projet.h"

wchar_t * desaccentuer(wchar_t *chaine){
	wchar_t * destination;
    wchar_t * pointer;
    int length = wcslen( chaine );
    
    destination = (wchar_t *) malloc( sizeof( wchar_t) * (length+1) );
    wcscpy( destination, chaine );
    if ( pointer = wcspbrk( destination, L"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïñòóôõöùúûüýÿ")){
		printf("Erreur caractère spéciaux détecté \n");
    while (pointer = wcspbrk( destination, L"ÈÉÊËèéèéêë" ) ) {
        *pointer = 'e';
    }
    while (pointer = wcspbrk( destination, L"ÀÁÂÃÄÅÆàáâãäå" ) ) {
        *pointer = 'a';
    }
	while ( pointer = wcspbrk( destination, L"Ññ")){
		*pointer = 'n';
	}
	while ( pointer = wcspbrk( destination, L"Çç")){
		*pointer = 'c';
	}
	while ( pointer = wcspbrk( destination, L"ÒÓÔÕÖòóôõö")){
		*pointer = 'o';
	}
	while ( pointer = wcspbrk( destination, L"ÌÍÎÏìíîï")){
		*pointer = 'i';
	}
	while ( pointer = wcspbrk( destination, L"ÙÚÛÜùúûü")){
		*pointer = 'u';
	}
	while ( pointer = wcspbrk( destination, L"Ýýÿ")){
		*pointer = 'u';
	}
	while ( pointer = wcspbrk( destination, L"Ð")){
		*pointer = 'd';
	}}
    printf( "Result : %ls\n", destination );
	
	return destination;
}