	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <math.h>
	#include <wchar.h>
	#include <locale.h>
	#include <wctype.h>
	#include<time.h>
	#include "projet.h"

int lire(wchar_t *chaine, int longueur)
{
    wchar_t *positionEntree = NULL;
 
    if (fgetws(chaine, longueur, stdin) != NULL)
    {
        positionEntree = wcschr(chaine, '\n');
        if (positionEntree != NULL)
        {
            *positionEntree = '\0';
        }
     
        return 1;
    }
    else
    {
       
        return 0;
    }
}