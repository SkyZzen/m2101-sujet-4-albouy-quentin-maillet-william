	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <math.h>
	#include <wchar.h>
	#include <locale.h>
	#include <wctype.h>
	#include<time.h>
	#include "projet.h"

void cesar(wchar_t *test){
	FILE* fichier = NULL;
	fichier = fopen("Resultat.txt", "a");
	wchar_t decalage[10];
	wchar_t reponse[10];
	wchar_t *end;
	wchar_t * mode;
	printf("Tapez (c) pour Crypter et (d) pour decrypter: \n");
	printf("---------------------------------------------\n");
	   
	lire(reponse,10);

    if (wcspbrk (reponse,L"cC")){
		printf("Veuilliez entrez le décalage \n");
		lire(decalage,10);
		long decalageLong = wcstol(decalage,&end,10);
		mode = L"crypter";
		if (fichier != NULL)
	{
    	fprintf(fichier,"Le texte va etre %ls .\n Le décalage utilisée est %ld .\n",mode,decalageLong);
    	fclose(fichier);
	}   
		cryptercesar(test , decalageLong);
		
	} else if ( wcspbrk (reponse,L"dD")){
		printf("Veuilliez entrez le décalage \n");
		lire(decalage,10);
		long  decalageLong = wcstol(decalage,&end,10);
		mode = L"decrypter";
		if (fichier != NULL)
	{
    	fprintf(fichier,"Le texte va etre %ls .\n Le décalage utilisée est %ld .\n",mode,decalageLong);
    	fclose(fichier);
	}   
		decryptercesar(test , decalageLong);
	} else {
		printf("Mauvais choix, Aurevoir !\n");
		exit(-1);
	}
}